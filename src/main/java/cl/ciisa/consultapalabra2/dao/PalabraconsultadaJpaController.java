/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.ciisa.consultapalabra2.dao;

import cl.ciisa.consultapalabra2.dao.exceptions.NonexistentEntityException;
import cl.ciisa.consultapalabra2.dao.exceptions.PreexistingEntityException;
import cl.ciisa.consultapalabra2.entity.Palabraconsultada;
import cl.entity.PalabraEntity;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author valen
 */
public class PalabraconsultadaJpaController implements Serializable {

    public PalabraconsultadaJpaController() {
        
    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("consultapalabra_PU");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Palabraconsultada palabraconsultada) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(palabraconsultada);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findPalabraconsultada(palabraconsultada.getId()) != null) {
                throw new PreexistingEntityException("Palabraconsultada " + palabraconsultada + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Palabraconsultada palabraconsultada) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            palabraconsultada = em.merge(palabraconsultada);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = palabraconsultada.getId();
                if (findPalabraconsultada(id) == null) {
                    throw new NonexistentEntityException("The palabraconsultada with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Palabraconsultada palabraconsultada;
            try {
                palabraconsultada = em.getReference(Palabraconsultada.class, id);
                palabraconsultada.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The palabraconsultada with id " + id + " no longer exists.", enfe);
            }
            em.remove(palabraconsultada);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Palabraconsultada> findPalabraconsultadaEntities() {
        return findPalabraconsultadaEntities(true, -1, -1);
    }

    public List<Palabraconsultada> findPalabraconsultadaEntities(int maxResults, int firstResult) {
        return findPalabraconsultadaEntities(false, maxResults, firstResult);
    }

    private List<Palabraconsultada> findPalabraconsultadaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Palabraconsultada.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Palabraconsultada findPalabraconsultada(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Palabraconsultada.class, id);
        } finally {
            em.close();
        }
    }

    public int getPalabraconsultadaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Palabraconsultada> rt = cq.from(Palabraconsultada.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public void create(PalabraEntity pal) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
