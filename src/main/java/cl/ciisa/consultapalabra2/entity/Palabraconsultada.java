/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.ciisa.consultapalabra2.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author valen
 */
@Entity
@Table(name = "palabraconsultada")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Palabraconsultada.findAll", query = "SELECT p FROM Palabraconsultada p"),
    @NamedQuery(name = "Palabraconsultada.findById", query = "SELECT p FROM Palabraconsultada p WHERE p.id = :id"),
    @NamedQuery(name = "Palabraconsultada.findByPalabra", query = "SELECT p FROM Palabraconsultada p WHERE p.palabra = :palabra"),
    @NamedQuery(name = "Palabraconsultada.findBySignificado", query = "SELECT p FROM Palabraconsultada p WHERE p.significado = :significado")})
public class Palabraconsultada implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "id")
    private String id;
    @Size(max = 20)
    @Column(name = "palabra")
    private String palabra;
    @Size(max = 100)
    @Column(name = "significado")
    private String significado;
    @Lob
    @Column(name = "fecha")
    private Object fecha;

    public Palabraconsultada() {
    }

    public Palabraconsultada(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPalabra() {
        return palabra;
    }

    public void setPalabra(String palabra) {
        this.palabra = palabra;
    }

    public String getSignificado() {
        return significado;
    }

    public void setSignificado(String significado) {
        this.significado = significado;
    }

    public Object getFecha() {
        return fecha;
    }

    public void setFecha(Object fecha) {
        this.fecha = fecha;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Palabraconsultada)) {
            return false;
        }
        Palabraconsultada other = (Palabraconsultada) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.ciisa.consultapalabra2.entity.Palabraconsultada[ id=" + id + " ]";
    }
    
}
