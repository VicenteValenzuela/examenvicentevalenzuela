/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.ciisa.consultapalabra2;

import cl.ciisa.consultapalabra2.dao.PalabraconsultadaJpaController;
import cl.dto.PalabraDTO;
import cl.entity.PalabraEntity;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.inject.New;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author valen
 */
@Path("diccionario")
public class ApiInternaDiccionario {
    
    @GET
    @Path("/{idbuscar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response significado( @PathParam("idbuscar") String idbuscar){
        
        
        
        try{
            
                    
        Client client =ClientBuilder.newClient();
        
        WebTarget myResource = client.target("https://od-api.oxforddictionaries.com/api/v2/entries/es/" + idbuscar);
   
        PalabraDTO palabradto = myResource.request(MediaType.APPLICATION_JSON).header("api-key","cb531756").header("api-id","b8a513f877abb786a5c7390050df427b").get(PalabraDTO.class );
    
        PalabraEntity pal = new PalabraEntity();
        
        pal.setPalabra(palabradto.getWord());
        Date fecha =new Date();
        pal.setFecha(fecha.toString());
        
        
        String significado = (String) palabradto.getWord();//   .getResults().get(0).getLexicalEntries().get(0).getEntries().get(0).getSenses().get(0).getDefinitions().toString();
            pal.setSignificado(significado);
                 
            
            
            PalabraconsultadaJpaController dao = new PalabraconsultadaJpaController();
            
            dao.create(pal);
            
            
             return Response.ok(200).entity(pal).build();
    }
    catch (Exception ex) {
            Logger.getLogger(ApiInternaDiccionario.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
}

}
