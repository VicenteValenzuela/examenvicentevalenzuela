/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.controller;

import cl.ciisa.consultapalabra2.dao.PalabraconsultadaJpaController;
import cl.ciisa.consultapalabra2.entity.Palabraconsultada;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author valen
 */
@WebServlet(name = "ConsultaPalabraController", urlPatterns = {"/ConsultaPalabraController"})
public class ConsultaPalabraController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ConsultaPalabraController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ConsultaPalabraController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
         
        String accion = request.getParameter("accion");
        String idbuscar = request.getParameter("idbuscar");
        
        if(accion.equals("buscar")) {
            Client client = ClientBuilder.newClient();
              WebTarget myResource = client.target("https://consultapalabra.herokuapp.com/api/diccionario/" + idbuscar);

              Palabraconsultada significado = (Palabraconsultada) myResource.request(MediaType.APPLICATION_JSON).get(new GenericType<Palabraconsultada>() {
            });
              
                try {
                String id = request.getParameter("id");
                String palabra = request.getParameter("palabra");
                String significados = request.getParameter("significado");
                String fecha = request.getParameter("fecha");
                
                Palabraconsultada palabras = new Palabraconsultada();
                palabras.setId(id);
                palabras.setPalabra(palabra);
                palabras.setSignificado(significados);   
                palabras.setFecha(fecha);
                
                
                PalabraconsultadaJpaController dao = new PalabraconsultadaJpaController();
                
                dao.create(palabras);
                 
               
            } catch (Exception ex) {
                Logger.getLogger(PalabraconsultadaJpaController.class.getName()).log(Level.SEVERE, null, ex);
            }
    
            
          
               request.setAttribute("significado", significado);
               request.getRequestDispatcher("paginaresultado.jsp").forward(request, response);
        }
        
        
        if (accion.equals("listar")) {
             Client client = ClientBuilder.newClient();
              WebTarget myResource = client.target("https://consultapalabra.herokuapp.com/api/diccionario/");

              List<Palabraconsultada> lista = (List<Palabraconsultada>) myResource.request(MediaType.APPLICATION_JSON).get(new GenericType<List<Palabraconsultada>>() {
            });
             
          
               request.setAttribute("listadoPalabras", lista);
               request.getRequestDispatcher("historial.jsp").forward(request, response);
            
        }
       
        if (accion.equals("volver")) {
            
            try {
                String id = request.getParameter("id");
                String palabra = request.getParameter("palabra");
                String significados = request.getParameter("significado");
                String fecha = request.getParameter("fecha");
                
                Palabraconsultada palabras = new Palabraconsultada();
                palabras.setId(id);
                palabras.setPalabra(palabra);
                palabras.setSignificado(significados); 
                palabras.setFecha(fecha);
                
                
                PalabraconsultadaJpaController dao = new PalabraconsultadaJpaController();
                
                dao.create(palabras);
                 
               
            } catch (Exception ex) {
                Logger.getLogger(PalabraconsultadaJpaController.class.getName()).log(Level.SEVERE, null, ex);
            }
             request.getRequestDispatcher("index.jsp").forward(request, response);
            
        } 
        
        
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void create(Palabraconsultada palabras) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
