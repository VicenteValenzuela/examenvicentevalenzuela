<%-- 
    Document   : index
    Created on : 06-05-2021, 21:50:22
    Author     : valen
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Ingrese su palabra a consultar</h1>
        
        <form  name="form" action="ConsultaPalabraController" method="POST">
            
            <input type="text" id="idbuscar" name="idbuscar">

            <button type="submit" name="accion" value="buscar" class="btn btn-success">Buscar</button>
            <button type="submit" name="accion" value="listar" class="btn btn-success">Listado Palabras</button>
        </form>
        
        <p>URL Bitbucket:  </p>
        
        <p>Vicente Valenzuela - Sección 50</p>
    </body>
</html>
