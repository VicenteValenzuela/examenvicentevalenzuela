<%-- 
    Document   : paginaresultado
    Created on : 08-05-2021, 21:40:17
    Author     : valen
--%>

<%@page import="cl.ciisa.consultapalabra2.entity.Palabraconsultada"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    Palabraconsultada significado = (Palabraconsultada) request.getAttribute("significado");
%>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>significado</h1>
        
        <form  name="form" action="ConsultaPalabraController" method="POST">
            <h1><%= significado.getPalabra().toString() %></h1>
            <h2><%= significado.getSignificado().toString() %></h2> 
            <h2>Fuente: Oxford Dictionaries</h2>
            <button type="submit" name="accion" value="volver" class="btn btn-success">Volver</button>
         </form>
    </body>
</html>
