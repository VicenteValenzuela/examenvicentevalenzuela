<%-- 
    Document   : historial
    Created on : 08-05-2021, 21:39:47
    Author     : valen
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="cl.ciisa.consultapalabra2.entity.Palabraconsultada"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
    List<Palabraconsultada> palabras = (List<Palabraconsultada>) request.getAttribute("listadoPalabras");
    Iterator<Palabraconsultada> itPalabras = palabras.iterator();
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Historial de palabras</h1>
        
        <form  name="form" action="ConsultaPalabraController" method="POST">

       
        <table border="1">
                    <thead>
                    <th>ID </th>
                    <th>Palabra</th>
                    <th>Significado</th>
                    <th>Fecha</th>
         
                    </thead>
                    <tbody>
                        <%while (itPalabras.hasNext()) {
                       Palabraconsultada cm = itPalabras.next();%>
                        <tr>
                            <td id="id_palabra"><%= cm.getId()%></td>
                            <td id="palabra"><%= cm.getPalabra()%></td>
                            <td id="significado"><%= cm.getSignificado()  %></td>
                            <td id="fecha"><%= cm.getFecha()%></td>
                         
                
                        </tr>
                        <%}%>                
                    </tbody>           
                </table>
                    <br><br>
     
            <button type="submit" name="accion" value="volver" class="btn btn-success">Volver</button>
        
         </form>   
                    
                    
    </body>
</html>
